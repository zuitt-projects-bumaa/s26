const http = require("http");

const port = 5000; 

const server = http.createServer((req,res) =>{ 
	console.log(req.url); //req url endpoint
	console.log(req.method)
	if(req.url === "/"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to B169 Booking System.')
	} else if(req.url === "/courses"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to the Courses Page. View our Courses.')
	} else if(req.url === "/profile"){
		res.writeHead(200, {'Content-Type' : 'text/plain'})
		res.end('Welcome to your Profile. View your details.')
	} else {
		res.writeHead(404, {'Content-Type' : 'text/plain'})
		res.end('Resource not Found')
	}
}); 

server.listen(5000);
console.log(`Server is now accessible at localhost: ${port}`)